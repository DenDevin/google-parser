<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_package}}`.
 */
class m200118_152746_create_user_package_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_package}}', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_package}}');
    }
}
