<?php

use yii\db\Migration;

/**
 * Class m200118_080631_proxy_table_migration
 */
class m200118_080631_proxy_table_migration extends Migration
{
    public function up()
    {
        $this->createTable('proxy_server', [
            'id' => $this->primaryKey(),
            'ip' => $this->string()->notNull(),
            'port' => $this->string()->notNull(),
            'password' => $this->string(),
            'country' => $this->string(),
            'status' => $this->integer()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('proxy_server');
    }
}
